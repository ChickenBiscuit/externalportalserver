# Makefile shamelessly "inspired" by https://github.com/helm/helm
GO        ?= go
TAGS      :=
LDFLAGS	  := -w -s
TESTS     := .
TESTFLAGS :=
GOFLAGS   :=
RUNFLAGS  :=
BINDIR    := $(CURDIR)/bin

GOOS := linux
GOARCH := amd64

#
# CI/CD Commands
#
all: main

main:
	export GOPRIVATE="git.dochq.co.uk/packages" && \
	git config --global url."https://gitlab-ci-token:${CI_REGISTRY_PASSWORD}@git.dochq.co.uk".insteadOf "https://git.dochq.co.uk"
	CGO_ENABLED=0 GOOS=$(shell uname -s | tr '[:upper:]' '[:lower:]') GOARCH=amd64 $(GO) build -a -ldflags="$(LDFLAGS)" -v ./cmd/main/main.go;

.PHONY: clean
clean:
	@rm -rf ./main

run: main
	sudo ./main
