package controllers

import (
	"net/http"

	"git.dochq.co.uk/packages/logging"
	"github.com/dim13/unifi"
)

var u *unifi.Unifi

func init() {
	var err error
	u, err = unifi.Login(
		"admin",
		"password",
		"192.168.0.2",
		"8443",
		"Default",
		5,
	)

	if err != nil {
		logging.Error(err)
	}
}

func Authenticate() func(http.ResponseWriter, *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {}
}
func ShowLoginForm() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "assets/html/index.html")
	}
}
