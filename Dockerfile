FROM golang:1.14-alpine AS build

ARG CI_REGISTRY_PASSWORD=aaaa

RUN apk update && apk upgrade && \
    apk add --no-cache bash git curl make gcc ca-certificates && \
    update-ca-certificates 2>/dev/null || true

ADD ./ /go/app

WORKDIR /go/app

RUN make

#
## Create final image
FROM scratch

COPY --from=build /go/app/main /opt/main
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY .env /opt/.env

WORKDIR /opt

ENTRYPOINT ["./main"]

EXPOSE 5000
