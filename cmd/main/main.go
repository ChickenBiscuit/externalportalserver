package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	hp "gitlab.com/ChickenBiscuit/blockchain/pkg/http"
	"gitlab.com/ChickenBiscuit/blockchain/pkg/logging"
	"gitlab.com/ChickenBiscuit/externalportalserver/controllers"

	"github.com/getsentry/sentry-go"
	"github.com/rs/cors"
)

func init() {
	if os.Getenv("DEBUG") == "true" {
		logging.DebugEnabled = true
	}
}

func main() {

	defer sentry.Flush(2 * time.Second)

	d := hp.New()

	d.PathPrefix("/css").Handler(http.StripPrefix("/css", http.FileServer(http.Dir("assets/css")))).Methods(http.MethodGet)
	d.HandleFunc("/", controllers.ShowLoginForm()).Methods(http.MethodGet)

	// Allow graceful exit by listening for terminate signal
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	corsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, // you service is available and allowed for this base url
		AllowedMethods: []string{
			http.MethodGet, // http methods for your app
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
		AllowedHeaders: []string{
			"*", // or you can your header key values which you are using in your application
		},
	})

	server := &http.Server{
		Addr:    os.Getenv("HTTP_PORT"),
		Handler: corsHandler.Handler(d)}
	logging.Info("Starting service...")

	sentry.CaptureMessage("It works!")

	canExit := false
	go func() {
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			logging.Info("ListenAndServe: Error" + err.Error())
		} else {
			logging.Info("Server gracefully stopped")
		}
		canExit = true
	}()

	logging.Info("Waiting for server shutdown signal...")

CronLoop:
	for {
		time.Sleep(time.Second)

		select {
		case <-stop:
			{
				logging.Info("Server is being shut down...")
				ctx, cancelFn := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancelFn()
				server.Shutdown(ctx)
			}

		default:
			if canExit {
				logging.Info("Server has been shut down")
				break CronLoop
			}

		}
	}

	// Finish gracefully
	logging.Info("Exiting process")
	os.Exit(0)
}
