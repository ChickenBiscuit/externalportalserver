module gitlab.com/ChickenBiscuit/externalportalserver

go 1.14

require (
	git.dochq.co.uk/packages/logging v0.0.0-20200522165624-3d7c1cd99349
	github.com/dim13/unifi v0.0.0-20200709055549-52998aa9c807
	github.com/getsentry/sentry-go v0.7.0 // indirect
	gitlab.com/ChickenBiscuit/blockchain v0.0.0-20200817145928-9ae48349673e // indirect
)
